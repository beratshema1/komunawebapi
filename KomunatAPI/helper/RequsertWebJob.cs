﻿using Komunat.data;
using KomunatAPI.Models;
using KomunatAPI.Services;
using Newtonsoft.Json;
using Quartz;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Web.Http;

namespace KomunatAPI.helper
{
    public class RequsetWebJob : IJob
    {
        //https://kk.rks-gov.net/wp-json/statusupdate/v1/status_update
        string Baseurl = "https://kk.rks-gov.net/";
        string url = "wp-json/statusupdate/v1/status_update";
        CertificationsService _certificationsService;

        public RequsetWebJob()
        {
            _certificationsService = new CertificationsService();
        }

        public void Execute(IJobExecutionContext context)
        {

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Clear();

                //Define request data format   
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var RCWebsDB = _certificationsService.RequestCertificateWebJob();

                HttpResponseMessage Res;
                var RCWebs = _certificationsService.RequestCertificateWeb(RCWebsDB);

                foreach (var item in RCWebsDB.CertificateWebJobViewModels)
                {

                    var rcweb = RCWebs.Where(x => x.Id == item.Id).FirstOrDefault();
                    try
                    {
                        item.HashPass = MD5Helper.GenerateHashPass(item);
                        ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                        Res = client.PostAsJsonAsync(url, item).Result;//krijimi i requestit me paramentra


                        var JobResponse = Res.Content.ReadAsStringAsync().Result;
                        if (JobResponse == "200")//nese eshte kthy edhe jane ruajt te dhenant
                        {
                            rcweb.ErrorNotifyLog = "";
                            rcweb.WebNotified = true;
                            rcweb.WebNotifiedDate = DateTime.Now;
                        }
                        else if (JobResponse == "404" || JobResponse == "401" || JobResponse == "400")//nese eshte kthy edhe nuk jane ruajt te dhenant
                        {
                            rcweb.WebNotified = false;
                            rcweb.ErrorNotifyLog = JobResponse;
                            rcweb.ErrorDateLog = DateTime.Now;
                        }
                        else
                        {
                            rcweb.WebNotified = false;
                            var error = Res.ToString() + ", RESPONSE ERROR = " + JobResponse;
                            rcweb.ErrorNotifyLog = error;
                            rcweb.ErrorDateLog = DateTime.Now;
                        }
                    }
                    catch (Exception ex)
                    {
                        rcweb.WebNotified = false;
                        var error = " EXCEPTION ERROR = " + ex.ToString();
                        rcweb.ErrorNotifyLog = error;
                        rcweb.ErrorDateLog = DateTime.Now;
                    }
                     
                }

                _certificationsService.Commit();

            }

        }


    }
}
