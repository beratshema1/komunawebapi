﻿using KomunatAPI.Models;
using System;
using System.Security.Cryptography;
using System.Text;

namespace KomunatAPI.helper
{
    public class MD5Helper
    {
        protected static string salt = "jrWC7SSuwFmdvkAFefJWcrzSVPayf8A7nSyyEANT";
        protected static string _cid, _mid, _pnr, _computedHash, _wrId;
        protected static byte[] bytes;

        public static string GenerateHashPass(CertificateWebJobViewModel item)
        {
            using (MD5 provider = MD5.Create())
            {
                _cid = item.Certificate_id.ToString();
                _mid = item.MunicipalityWebId.ToString();
                _pnr = item.NUMRI_PERSONAL;
                _wrId = item.WEB_REQUEST_ID.ToString();
                string all = _cid + _mid + _pnr + _wrId + salt;
                bytes = provider.ComputeHash(Encoding.ASCII.GetBytes(_cid + _mid + _pnr + _wrId + salt));
                _computedHash = BitConverter.ToString(bytes);
            }
            
            return _computedHash.Replace("-", "");
        }
    }
}