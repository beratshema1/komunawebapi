﻿using Quartz;
using Quartz.Impl;
using System.Configuration;

namespace KomunatAPI.helper
{
    public class JobScheduler
    {
        public static string WITH_INTERVAL_IN_MINUTES = ConfigurationManager.AppSettings["WITH_INTERVAL_IN_MINUTES"];

        public static void Start()
        {

            int.TryParse(WITH_INTERVAL_IN_MINUTES, out int valueInMinute);

            if (valueInMinute <= 0)
                valueInMinute = 30;

            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<RequsetWebJob>().Build(); 

            ITrigger trigger = TriggerBuilder.Create()
                                    .WithIdentity("CallWebService_WebInMinnute", "RequestsCertificate")
                                    .StartNow()
                                    .WithSimpleSchedule(x => x
                                        .WithIntervalInMinutes(valueInMinute)
                                        .RepeatForever())
                                    .Build();

            scheduler.ScheduleJob(job, trigger);
        }
    }
}