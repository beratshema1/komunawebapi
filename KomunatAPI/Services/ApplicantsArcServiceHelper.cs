﻿using Komunat.data;
using KomunatAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KomunatAPI.Services
{
    public class ApplicantsArcServiceHelper
    {
        CertificationsService _certificationsService;
        

        public ApplicantsArcServiceHelper()
        {
            _certificationsService = new CertificationsService(); 
        }

        private async Task<ApplicantWebServiceGetViewModel> GetArcApplicant(string personalNr, int _web_municipalityId, bool CIVIL_REGISTRY_DUMMY)
        {
            ApplicantWebServiceGetViewModel person = new ApplicantWebServiceGetViewModel();

            if (CIVIL_REGISTRY_DUMMY)
            {
                var applicanArc = await GetList();
                return applicanArc.Where(x => x.NUMRI_PERSONAL == personalNr).FirstOrDefault();
            }
            //ketu applicaktut duhet me qene prej webservisit te ARC-s
            try
            {


                int mid = await _certificationsService.GetMunicipalityByMWebId(_web_municipalityId);
                KomunatAPI.ARCWebService.KerkoQytetarinSoapClient arcWebService = new ARCWebService.KerkoQytetarinSoapClient();
                
                DataSet dts = arcWebService.Kerko(personalNr, mid, 1, "QSHQ");

                if (dts.Tables.Count > 0 && dts.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = dts.Tables[0].Rows[0];



                    person.NUMRI_PERSONAL = dr["NUMRI_PERSONAL"].ToString();
                    person.EMRI = dr["EMRI"].ToString();
                    person.MBIEMRI = dr["MBIEMRI"].ToString();
                    person.EMRI_MBIEMRI_BABA = dr["EMRI_MBIEMRI_BABA"].ToString();
                    person.ADRESA_E_VENDBANIMIT = dr["ADRESA_E_VENDBANIMIT"].ToString();
                    person.VENDLINDJA = dr["VENDLINDJA"].ToString();
                    person.DATLINDJA = dr["DATLINDJA"].ToString(); //DateTime.TryParse(dr["DATLINDJA"].ToString(), out BirthDate) == true ? BirthDate : (DateTime?)null;
                    person.KAMARTESE = dr["KAMARTESE"].ToString();
                    person.GJINIA = dr["GJINIA"].ToString();
                    person.GJENDJA_MARTESORE = dr["GJENDJA_MARTESORE"].ToString();
                    person.SHTETESIA = dr["SHTETESIA"].ToString();
                    person.DATELINDJA_BABA = dr["DATELINDJA_BABA"].ToString();//DateTime.TryParse(dr["DATELINDJA_BABA"].ToString(), out BirthDate) == true ? BirthDate : (DateTime?)null;
                    person.VENDLINDJA_BABA = dr["vENDLINDJA_BABA"].ToString();
                    person.EMRI_MBIEMRI_NENA = dr["EMRI_MBIEMRI_NENA"].ToString();
                    person.DATELINDJA_NENA = dr["DATELINDJA_NENA"].ToString();// DateTime.TryParse(dr["DATELINDJA_NENA"].ToString(), out BirthDate) == true ? BirthDate : (DateTime?)null;
                    person.VENDLINDJA_NENA = dr["vENDLINDJA_NENA"].ToString();
                    person.VERIFIKUAR = dr["VERIFIKUAR"].ToString();
                    person.KALINDJE = dr["KALINDJE"].ToString();
                    person.KAVDEKJE = dr["KAVDEKJE"].ToString();
                }



                if (!person.Validate())
                {
                    person = null;
                    return person;
                }

                return person;

            }
            catch (Exception ex)
            {

            }

            return person;
        }

        public async Task<ApplicantWebServiceGetViewModel> GetApplicant(string personalNr, string _web_municipalityId, bool CIVIL_REGISTRY_DUMMY)
        {
            int web_municipalityId = 0;
            int.TryParse(_web_municipalityId, out web_municipalityId);
            var applicanArc = await GetArcApplicant(personalNr, web_municipalityId, CIVIL_REGISTRY_DUMMY);

            if (applicanArc == null)
            {
                return null;
            }

            var cerlist = await _certificationsService.GetListAsync(web_municipalityId);

            if (applicanArc.KALINDJE.ToLower().Equals("false"))
            {
                cerlist = cerlist.Where(x => x.Id != 1 && x.Id != 7).ToList();
            }

            if (applicanArc.KAMARTESE.ToLower().Equals("false"))
            {
                cerlist = cerlist.Where(x => x.Id != 4).ToList();
            }

            if (applicanArc.KAVDEKJE.ToLower().Equals("false"))
            {
                cerlist = cerlist.Where(x => x.Id != 2).ToList();
            }

            applicanArc.CertificateItems = cerlist.ToList();

            return applicanArc;
        }

        public async Task<RequestWeb> GetRequestWeb(ApplicantWebServicePostViewModel item)
        {
          //  DateTime tempDate = DateTime.ParseExact(item.DATLINDJA, "MM/dd/yyyy", CultureInfo.InvariantCulture);
            //DateTime.TryParse(item.DATLINDJA,out tempDate);
            RequestWeb objrw = new RequestWeb
            {
                NUMRI_PERSONAL = item.NUMRI_PERSONAL,
                WEB_REQUEST_ID = (int)item.WEB_REQUEST_ID,
                EMRI = item.EMRI,
                MBIEMRI = item.MBIEMRI,
                EMRI_MBIEMRI_BABA = item.EMRI_MBIEMRI_BABA,
                ADRESA_E_VENDBANIMIT = item.ADRESA_E_VENDBANIMIT,
                VENDLINDJA = item.VENDLINDJA,
                DATLINDJA = item.DATLINDJA,
                DATELINDJA_NENA = item.DATELINDJA_NENA,
                GJINIA = item.GJINIA,
                GJENDJA_MARTESORE = item.GJENDJA_MARTESORE,
                SHTETESIA = item.SHTETESIA,
                DATELINDJA_BABA = item.DATELINDJA_BABA,
                VENDLINDJA_BABA = item.VENDLINDJA_BABA,
                EMRI_MBIEMRI_NENA = item.EMRI_MBIEMRI_NENA,
                VENDLINDJA_NENA = item.VENDLINDJA_NENA,
                VERIFIKUAR = item.VERIFIKUAR,
                KALINDJE = item.KALINDJE,
                KAVDEKJE = item.KAVDEKJE,
                KAMARTESE = item.KAMARTESE,
                DELIVERED_ADDRESS = item.DELIVERED_ADDRESS,
                MunicipalityWebId = item.MunicipalityWebId,
                KOMUNA = item.KOMUNA,
                ADDRESSA = item.ADDRESSA,
                TELFAX = item.TELFAX,
                TELMOBILE = item.TELMOBILE,
                EMAIL = item.EMAIL
            };

            return await Task.Run(() => objrw);
        }

        private async Task<List<ApplicantWebServiceGetViewModel>> GetList()
        {
            var list = new List<ApplicantWebServiceGetViewModel>
            {
                new ApplicantWebServiceGetViewModel
                {
                    NUMRI_PERSONAL = "123",
                    EMRI = "Drilon",
                    MBIEMRI = "Jahiri",
                    DATLINDJA = "06/06/1986",
                    GJINIA = "1",
                    VENDLINDJA = "Prishtine",
                    GJENDJA_MARTESORE = "i,e martuar/oženjen/married",
                    SHTETESIA = "Kosovar",
                    ADRESA_E_VENDBANIMIT = "Tring Smajli nr 5",
                    EMRI_MBIEMRI_BABA = "A",
                    DATELINDJA_BABA = "05/09/1960",
                    EMRI_MBIEMRI_NENA = "A",
                    DATELINDJA_NENA = "05/09/1950",
                    KALINDJE = "TRUE",
                    KAMARTESE = "TRUE",
                    KAVDEKJE = "FALSE",
                    VENDLINDJA_BABA = "Presheve",
                    VENDLINDJA_NENA = "Presheve",
                    VERIFIKUAR = "1",
                KOMUNA  = "Prishtine",
                ADDRESSA = "Prishtine, kosove",
                TELFAX = "",
                TELMOBILE = "",
                EMAIL = ""
                },
                new ApplicantWebServiceGetViewModel
                {
                    NUMRI_PERSONAL = "111",
                    EMRI = "Berat",
                    MBIEMRI = "Shema",
                    DATLINDJA = "06/06/1980",
                    GJINIA = "1",
                    VENDLINDJA = "Prishtine",
                    GJENDJA_MARTESORE = "i,e martuar/oženjen/married",
                    SHTETESIA = "Kosovar",
                    ADRESA_E_VENDBANIMIT = "Agim Ramadani nr 670",
                    EMRI_MBIEMRI_BABA = "A",
                    DATELINDJA_BABA = "05/09/1960",
                    EMRI_MBIEMRI_NENA = "A",
                    DATELINDJA_NENA = "05/09/1950",
                    KALINDJE = "FALSE",
                    KAMARTESE = "FALSE",
                    KAVDEKJE = "TRUE",
                    VENDLINDJA_BABA = "Prishtine",
                    VENDLINDJA_NENA = "Prishtine",
                    VERIFIKUAR = "1",
                KOMUNA  = "Prishtine",
                ADDRESSA = "Prishtine, kosove",
                TELFAX = "",
                TELMOBILE = "",
                EMAIL = ""
                },
                new ApplicantWebServiceGetViewModel
                {
                    NUMRI_PERSONAL = "222",
                    EMRI = "Atifete",
                    MBIEMRI = "Jahjaga",
                    DATLINDJA = "04/03/1980",
                    GJINIA = "0",
                    VENDLINDJA = "Prishtine",
                    GJENDJA_MARTESORE = "i,e martuar/oženjen/married",
                    SHTETESIA = "Kosovar",
                    ADRESA_E_VENDBANIMIT = "Prishtin pn",
                    EMRI_MBIEMRI_BABA = "A",
                    DATELINDJA_BABA = "05/09/1940",
                    EMRI_MBIEMRI_NENA = "A",
                    DATELINDJA_NENA = "04/09/1940",
                    KALINDJE = "TRUE",
                    KAMARTESE = "FALSE",
                    KAVDEKJE = "FALSE",
                    VENDLINDJA_BABA = "Prishtine",
                    VENDLINDJA_NENA = "Prishtine",
                    VERIFIKUAR = "1",
                KOMUNA  = "Prishtine",
                ADDRESSA = "Prishtine, kosove",
                TELFAX = "",
                TELMOBILE = "",
                EMAIL = ""
                },
                new ApplicantWebServiceGetViewModel
                {
                    NUMRI_PERSONAL = "333",
                    EMRI = "Filan",
                    MBIEMRI = "Fisteku",
                    DATLINDJA = "04/03/1980",
                    GJINIA = "1",
                    VENDLINDJA = "Prishtine",
                    GJENDJA_MARTESORE = "i,e martuar/oženjen/married",
                    SHTETESIA = "Kosovar",
                    ADRESA_E_VENDBANIMIT = "Skenderbegu nr 17",
                    EMRI_MBIEMRI_BABA = "BB",
                    DATELINDJA_BABA = "05/09/1940",
                    EMRI_MBIEMRI_NENA = "BB",
                    DATELINDJA_NENA = "04/09/1940",
                    KALINDJE = "TRUE",
                    KAMARTESE = "FALSE",
                    KAVDEKJE = "FALSE",
                    VENDLINDJA_BABA = "Prishtine",
                    VENDLINDJA_NENA = "Prishtine",
                    VERIFIKUAR = "0",
                    KOMUNA  = "Prishtine",
                    ADDRESSA = "Prishtine, kosove",
                    TELFAX = "",
                    TELMOBILE = "",
                    EMAIL = ""
                }
            };

            return await Task.Run(() => list);
        }
    }
}