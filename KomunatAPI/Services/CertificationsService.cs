﻿using Komunat.data;
using KomunatAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;

namespace KomunatAPI.Services
{
    public class CertificationsService : IDisposable
    {
        KomunatContext db;

        public CertificationsService()
        {
            // db = new KomunatContext();
        }

        public void Commit()
        {
            db.SaveChanges();
        }

        private IQueryable<MunicipalityServices> GetAllList(KomunatContext db)
        {
            return db.MunicipalityServiceses
                        .Include("Certificate")
                        .Include("Municipality")
                        .AsNoTracking();
            //  .Where(x => x.active == true);
        }

        /// <summary>
        /// Kthen Listen e paramentave te cilet mandej kan me u perdor per Web Service te Web-it
        /// </summary>
        /// <returns> kthen Listen e paramentave te cilet mandej kan me u perdor per Web Service te Web-it</returns>
        public CertificateWebJobViewModelList RequestCertificateWebJob()
        {
            CertificateWebJobViewModelList objrw = new CertificateWebJobViewModelList();
            try
            {
                using (KomunatContext db = new KomunatContext())
                {
                    objrw.CertificateWebJobViewModels = db.Database.SqlQuery<CertificateWebJobViewModel>("[dbo].[sp_NotifyWeb]").ToList();
                }
            }
            catch (Exception e) { }

            return objrw;
        }

        /// <summary>
        /// Listen e RequestCertificateWeb sipas parametrit
        /// </summary>
        /// <param name="CRWebjob">Paramentin te cilin e kthen procedura per me u thirr webservice, i njejti perdoret per te konsumuar Listn RequestsCertificateWeb</param>
        /// <returns>Listen e RequestCertificateWeb sipas parametrit</returns> 
        public List<RequestCertificateWeb> RequestCertificateWeb(CertificateWebJobViewModelList CRWebjob)
        {
            //using (KomunatContext db = new KomunatContext())
            {

                if (db != null) db.Dispose();

                db = new KomunatContext();

                List<int> listRCWebIds = CRWebjob.CertificateWebJobViewModels != null ?
                                                   CRWebjob.CertificateWebJobViewModels.
                                                   Select(x => x.Id).
                                                   ToList() :
                                                   new List<int>();

                List<RequestCertificateWeb> objrw = new List<RequestCertificateWeb>();

                if (listRCWebIds == null)
                    listRCWebIds = new List<int>();

                try
                {
                    objrw = db.RequestCertificateWebs.Where(x => listRCWebIds.Contains(x.Id)).
                                                           ToList();
                }
                catch { }

                return objrw;
            }
        }
        public async Task<int> GetMunicipalityByMWebId(int Web_MunicipalityId)
        {
            int mid;
            using (KomunatContext db = new KomunatContext())
            {
                mid = await db.Municipalities.AsNoTracking().
                                                Where(c => c.webApiId == Web_MunicipalityId).
                                                Select(x => x.id).
                                                FirstOrDefaultAsync();

            }
            return mid;
        }

        public async Task<ICollection<CertificateItem>> GetListAsync(int Web_MunicipalityId)
        {
            using (KomunatContext db = new KomunatContext())
            {
                var mid = await db.Municipalities.AsNoTracking().
                                                  Where(c => c.webApiId == Web_MunicipalityId).
                                                  Select(x => x.id).
                                                  FirstOrDefaultAsync();

                List<CertificateItem> list = new List<CertificateItem>();

                list = GetAllList(db)
                           .Where(x => x.Municipality.id == mid && x.OutsideRequestPrice > 0)
                           .Select(x => new CertificateItem
                           {
                               Id = x.Certificate.id,
                               Value = x.OutsideRequestPrice.ToString()
                           })
                           .Where(x => x.Id != null)
                           .ToList();

                return list;
            }

        }

        public async Task AddRequest(RequestWeb objrw, int MunicipalityWebId)
        {
            using (KomunatContext db = new KomunatContext())
            {
                objrw.MunicipalityId = db.Municipalities.
                                        AsNoTracking().
                                        Where(x => x.webApiId == MunicipalityWebId).
                                        FirstOrDefault().id;

                db.RequestWebs.Add(objrw);
                await db.SaveChangesAsync();

            }

        }

        public async Task AddCertificateRequests(ApplicantWebServicePostViewModel item, int requestweb_id)
        {
            using (KomunatContext con = new KomunatContext())
            {
                List<RequestCertificateWeb> RequestCertificateWebs = new List<RequestCertificateWeb>();
                var requestweb = con.RequestWebs.Where(c => c.ID == requestweb_id).FirstOrDefault();

                RequestCertificateWebs = ConvertCertificateItemToRequestCertificate(item, con, requestweb);

                con.RequestCertificateWebs.AddRange(RequestCertificateWebs);
                await con.SaveChangesAsync();

            }
        }

        private static List<RequestCertificateWeb> ConvertCertificateItemToRequestCertificate(ApplicantWebServicePostViewModel item,
                                                                                              KomunatContext con,
                                                                                              RequestWeb requestweb)
        {
            List<RequestCertificateWeb> RequestCertificateWebs = new List<RequestCertificateWeb>();
            foreach (var ci in item.CertificateItems)
            {
                var certificate = con.Certificates.Where(c => c.id == ci.Id).FirstOrDefault();

                int certificateid = certificate.id;
                RequestCertificateWeb requestCertificateWeb = new RequestCertificateWeb()
                {
                    Certificate = certificate,
                    PaymentValues = decimal.Parse(ci.Value),
                    RequestWeb = requestweb
                };

                RequestCertificateWebs.Add(requestCertificateWeb);
            }
            return RequestCertificateWebs;
        }

        public async Task AddRequestCertificateWeb(List<RequestCertificateWeb> objrw)
        {
            using (KomunatContext db = new KomunatContext())
            {
                db.RequestCertificateWebs.AddRange(objrw);
                await db.SaveChangesAsync();
            }

        }

        public async Task AddRequestCertificate(ICollection<RequestCertificateWeb> objrw)
        {
            using (KomunatContext db = new KomunatContext())
            {
                db.RequestCertificateWebs.AddRange(objrw);
                await db.SaveChangesAsync();
            }
        }

        public void Dispose()
        {
            if (db != null) db.Dispose();

        }


    }
}