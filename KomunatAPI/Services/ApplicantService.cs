﻿using Komunat.data;
using KomunatAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;



namespace KomunatAPI.Services
{
    public class ApplicantService
    {
        private KomunatContext db = new KomunatContext();
         
        public IList<ApplicantRequest> GetRequests(string RequestNumber, string MunicipalityId = "1")
        { 
            IList<ApplicantRequest> result = new List<ApplicantRequest>();

            try
            {
                result = db.Database.SqlQuery<ApplicantRequest>("[spRequestsByApplicant] @PersonalNr, @MunicipalityId ",
                                                                new SqlParameter("@PersonalNr ", RequestNumber),
                                                                new SqlParameter("@MunicipalityId", MunicipalityId)
                                                        ).ToList();
            }
            catch(Exception e) { }

            return result; 
        }

    }
}