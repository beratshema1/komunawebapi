﻿using Komunat.data;
using KomunatAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace KomunatAPI.Services
{
    public class RequestService
    {
        private KomunatContext db = new KomunatContext();

        public async Task<Request> GetRequestByProtocolNr(string PNr, int MId, string PrNr)
        {
            Request result = new Request();

            try
            {
                result = await db.Database.SqlQuery<Request>("[spRequestByProtcolNr] @ProtocorNr, @MunicipalityId, @PersonalNr",
                                                                new SqlParameter("@ProtocorNr ",  PNr),
                                                                new SqlParameter("@MunicipalityId",  MId),
                                                                new SqlParameter("@PersonalNr", PrNr)
                                                        ).FirstOrDefaultAsync();
            }
            catch (Exception e) { }

            return result;
        }
    }
}