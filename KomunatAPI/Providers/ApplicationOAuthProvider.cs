﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Komunat.data;
using KomunatAPI;
using KomunatAPI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;


namespace KomunatAPI.web.Providers
{
    // Eshte shtuar per Web API
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;
        private string ClientID = "15920d2f-b529-4d9c-90d5-f6358e2f420e";
        private string KomunaApiUser = "ApiUser";
        public UserManager<ApplicationUser> UserManager { get; private set; }

        public static Func<UserManager<ApplicationUser>> UserManagerFactory { get; set; }

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }
             
            _publicClientId = publicClientId;
            UserManager= new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new KomunatContext()));
        }
         

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            AppUser appUser = new AppUser("API");  
            var userManager = appUser.userManager; 
            var user =   await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }
            // TO DO: me vone te shtohet ClientID te user i Intraneti-se. Nese ClientID e derguar nga app i intraneti eshte ndryshe nga ClientID ne bazen tone, mos e lejo...
            if (user.UserName != KomunaApiUser)
            {
                context.SetError("invalid_grant", "Only authorized users are allowed.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            AuthenticationProperties properties = CreateProperties(user.UserName);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId = "";
            string clientSecret = "";

            context.TryGetFormCredentials(out clientId, out clientSecret);

            // Te ndryshohet me vone ne nje ID te gjeneruar nga ne per ZKM-ne. Pastaj te kontrollohet nese perdoruesi e ka kete ID
            if (context.ClientId == ClientID)
            {
                this.ClientID = context.ClientId;
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }
    }
}