﻿using KomunatAPI.helper;
using KomunatAPI.Models;
using KomunatAPI.Services;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Http;

namespace KomunatAPI.Controllers
{
    [ApiVersion("0.0")] 
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [ApiVersion("3.0")]
    [Route("api/v{version:apiVersion}/ApplicantsArc")]
    public class ApplicantsArcController : ApiController
    {

        private ApplicantsArcServiceHelper applicant;
        private CertificationsService _certificationsService;

        private bool CIVIL_REGISTRY_DUMMY = bool.Parse(WebConfigurationManager.AppSettings["CIVIL_REGISTRY_DUMMY"]);
        public ApplicantsArcController()
        {
            applicant = new ApplicantsArcServiceHelper();
            _certificationsService = new CertificationsService();
        }
        [MapToApiVersion("0.0")]
        [HttpGet()]
        public async Task<IHttpActionResult> Ping()
        {
            return Ok("pinged");
        }

        /// <summary>
        /// eshte vetem per testim
        /// </summary>
        /// <returns></returns>
        [MapToApiVersion("3.0")]
        [HttpGet()]
        public async Task<IHttpActionResult> TestApiClientGetV3()
        {
            using (var client = new HttpClient())
            {
                //https://kk.rks-gov.net/wp-json/statusupdate/v1/status_update
                string Baseurl = "https://kk.rks-gov.net/";
                string url = "wp-json/statusupdate/v1/status_update";
                //Passing service base url  

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Clear();

                //Define request data format   
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var RCWebsDB = _certificationsService.RequestCertificateWebJob();

                HttpResponseMessage Res;
                var RCWebs = _certificationsService.RequestCertificateWeb(RCWebsDB);

                foreach (var item in RCWebsDB.CertificateWebJobViewModels)
                { 
                    var rcweb = RCWebs.Where(x => x.Id == item.Id).FirstOrDefault();

                    try
                    {
                        item.HashPass = MD5Helper.GenerateHashPass(item);
                        ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                        Res = client.PostAsJsonAsync(url, item).Result;//krijimi i requestit me paramentra


                        var JobResponse = Res.Content.ReadAsStringAsync().Result;
                        if (JobResponse == "200")//nese eshte kthy edhe jane ruajt te dhenant
                        {
                            rcweb.WebNotified = true;
                            rcweb.WebNotifiedDate = DateTime.Now;
                        }
                        else if (JobResponse == "404" || JobResponse == "401" || JobResponse == "400")//nese eshte kthy edhe jan ruajt te dhenant
                        {
                            rcweb.WebNotified = false;
                            rcweb.ErrorNotifyLog = JobResponse;
                            rcweb.ErrorDateLog = DateTime.Now;
                        }
                        else
                        {
                            rcweb.WebNotified = false;
                            var error = Res.ToString() + ", RESPONSE ERROR = " + JobResponse;
                            rcweb.ErrorNotifyLog = error;
                            rcweb.ErrorDateLog = DateTime.Now;
                        }
                    }
                    catch (Exception ex)
                    {
                        rcweb.WebNotified = false;
                        var error = " EXCEPTION ERROR = " + ex.ToString();
                        rcweb.ErrorNotifyLog = error;
                        rcweb.ErrorDateLog = DateTime.Now;
                    }
                }

                _certificationsService.Commit();

            }
            return null;
        }



        [MapToApiVersion("1.0")]
        [HttpGet()]
        public async Task<IHttpActionResult> Get(string PNr, string MId)
        {
            try
            {
                if (string.IsNullOrEmpty(PNr))
                    return BadRequest(PNr);

                if (string.IsNullOrEmpty(MId) || !int.TryParse(MId, out int MunicipalityId))
                    return BadRequest("Input 'MId' are not valid");

                var requests = await applicant.GetApplicant(PNr, MId, CIVIL_REGISTRY_DUMMY);

                if (requests == null)
                    return NotFound();

                return Ok(requests);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }


        [MapToApiVersion("2.0")]
        [HttpGet()]
        [Authorize]
        public async Task<IHttpActionResult> GetV2(string PNr, string MId)
        {
            try
            {
                if (string.IsNullOrEmpty(PNr))
                    return BadRequest(PNr);

                if (string.IsNullOrEmpty(MId) ||  !int.TryParse(MId, out int MunicipalityId))
                    return BadRequest("Input 'MId' are not valid");

                var requests = await applicant.GetApplicant(PNr, MId, CIVIL_REGISTRY_DUMMY);

                if (requests == null)
                    return NotFound();

                return Ok(requests);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }

        [MapToApiVersion("1.0")]
        [HttpPost()] 
        public async Task<IHttpActionResult> Post([FromBody]ApplicantWebServicePostViewModel item)
        {
            try
            {
                if (item.Validate())
                {
                    var webrequests = await applicant.GetRequestWeb(item);
                    await _certificationsService.AddRequest(webrequests, item.MunicipalityWebId);
                    await _certificationsService.AddCertificateRequests(item, webrequests.ID);
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });

            }

        }

        [MapToApiVersion("2.0")]
        [HttpPost()]
        [Authorize]
        public async Task<IHttpActionResult> PostV2([FromBody]ApplicantWebServicePostViewModel item)
        {
            try
            {
                if (item.Validate())
                {
                    var webrequests = await applicant.GetRequestWeb(item);
                    await _certificationsService.AddRequest(webrequests, item.MunicipalityWebId);
                    await _certificationsService.AddCertificateRequests(item, webrequests.ID);
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });

            }

        }

    }
}
