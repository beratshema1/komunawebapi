﻿using Komunat.data;
using KomunatAPI.Models;
using KomunatAPI.Services;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace KomunatAPI.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/Requests")]
    public class RequestsController : ApiController
    {
        private RequestService _requestService = new RequestService();
        // GET: api/Requests
        [HttpGet()]
        [MapToApiVersion("1.0")]
        public async Task<IHttpActionResult> Get(string PNr, string MId, string PrNr)
        {
            try
            {
                int MunicipalityId = 0;

                if (string.IsNullOrEmpty(PNr))
                    return BadRequest("Input 'PNr' are not valid");
                else if (string.IsNullOrEmpty(PrNr))
                    return BadRequest("Input 'PrNr' are not valid");
                else if (string.IsNullOrEmpty(MId) || !int.TryParse(MId, out MunicipalityId))
                    return BadRequest("Input 'MId' are not valid");


                var requests = await _requestService.GetRequestByProtocolNr(PNr, MunicipalityId, PrNr);

                if (requests == null ||
                    requests.Id == 0)
                    return NotFound();

                return Ok(requests);
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }

        [HttpGet()]
        [Authorize]
        [MapToApiVersion("2.0")]
        public async Task<IHttpActionResult> GetV2(string PNr, string MId, string PrNr)
        {
            try
            {

                int MunicipalityId = 0;

                if (string.IsNullOrEmpty(PNr))
                    return BadRequest("Input 'PNr' are not valid");
                else if (string.IsNullOrEmpty(PrNr))
                    return BadRequest("Input 'PrNr' are not valid");
                else if (string.IsNullOrEmpty(MId) || !int.TryParse(MId, out MunicipalityId))
                    return BadRequest("Input 'MId' are not valid");

                var requests = await _requestService.GetRequestByProtocolNr(PNr, MunicipalityId, PrNr);

                if (requests == null ||
                    requests.Id == 0)
                    return NotFound();

                return Ok(requests);
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });
            }
        }
         
    }
}
