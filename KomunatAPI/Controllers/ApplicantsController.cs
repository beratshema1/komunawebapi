﻿using Komunat.data;
using KomunatAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using KomunatAPI.Services;

namespace KomunatAPI.Controllers
{
    public class ApplicantsController : ApiController
    {
        private KomunatContext db = new KomunatContext();


        /// <summary>
        /// GET: api/Applicant/GetRequests/{RNumber}
        /// </summary>
        /// <param name="RNumber"></param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<IHttpActionResult> Get(string RNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(RNumber))
                    return BadRequest(RNumber);

                ApplicantService applicant = new ApplicantService();

                var requests = applicant.GetRequests(RNumber);

                if (requests == null || requests.Count() == 0)
                    return NotFound();

                return Ok(requests);
            }
            catch
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    Content = new StringContent("An error occurred"),
                    ReasonPhrase = "Critical Exception"
                });
            }

        }

    }
}
