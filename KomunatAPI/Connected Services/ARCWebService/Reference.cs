﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KomunatAPI.ARCWebService {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.dataprognet-ks.com", ConfigurationName="ARCWebService.KerkoQytetarinSoap")]
    public interface KerkoQytetarinSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.dataprognet-ks.com/Kerko", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet Kerko(string NumriPersonal, int KodiKomunes, int Gjuha, string Arsyeja);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://www.dataprognet-ks.com/Kerko", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> KerkoAsync(string NumriPersonal, int KodiKomunes, int Gjuha, string Arsyeja);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface KerkoQytetarinSoapChannel : KomunatAPI.ARCWebService.KerkoQytetarinSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class KerkoQytetarinSoapClient : System.ServiceModel.ClientBase<KomunatAPI.ARCWebService.KerkoQytetarinSoap>, KomunatAPI.ARCWebService.KerkoQytetarinSoap {
        
        public KerkoQytetarinSoapClient() {
        }
        
        public KerkoQytetarinSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public KerkoQytetarinSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public KerkoQytetarinSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public KerkoQytetarinSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Data.DataSet Kerko(string NumriPersonal, int KodiKomunes, int Gjuha, string Arsyeja) {
            return base.Channel.Kerko(NumriPersonal, KodiKomunes, Gjuha, Arsyeja);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> KerkoAsync(string NumriPersonal, int KodiKomunes, int Gjuha, string Arsyeja) {
            return base.Channel.KerkoAsync(NumriPersonal, KodiKomunes, Gjuha, Arsyeja);
        }
    }
}
