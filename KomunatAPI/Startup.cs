﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KomunatAPI.Startup))]
namespace KomunatAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //app.UseJwtBearerAuthentication(new JwtBearerOptions
            //{
            //    Authority = "https://login.windows.net/common",
            //    TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuer = false, // The issuer may vary in a multitenant scenario.
            //        ValidateAudience = false, // Allowing passing a token among multiple services (audiences).
            //    }
            //});
        }
    }
}
