﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KomunatAPI.Models
{
    public class SubjectStatus
    {
        public int ID { get; set; }
        public string NameAl { get; set; }

        public string NameEn { get; set; }

        public string NameSr { get; set; }
    }
}