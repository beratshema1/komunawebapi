﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KomunatAPI.Models
{
    public class CertificateItem
    {
        public int? Id { get; set; }
        public string Value { get; set; }
    }
}