﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KomunatAPI.Models
{
    public class ApplicantRequest
    {
        public int Id { get; set; }
        public string ProtocolNr { get; set; }
        public string DataPageses { get; set; }
        public string DateClosed { get; set; }
        public string CaseDescription { get; set; }
        public string Status { get; set; }
        public string AreaId { get; set; }

    }
}