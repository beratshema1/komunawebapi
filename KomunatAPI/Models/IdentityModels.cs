﻿using System;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Komunat.data;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace KomunatAPI.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
   
    public class ApplicationUser  : AppUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
           
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class KomunaApiDbContext : IdentityDbContext<ApplicationUser>
    {

      

        public KomunaApiDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
           
        }

        public static KomunaApiDbContext Create()
        {
          
            return new KomunaApiDbContext();
        }
    }
}