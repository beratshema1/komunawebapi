﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KomunatAPI.Models
{
    public class Request
    {
        public int Id { get; set; }
        public string ProtocolNr { get; set; }
        public string DataCreated { get; set; }
        public string DateClosed { get; set; }
        public string caseDescription { get; set; }
        public int Status { get; set; }
        public int MunicipalityId { get; set; }
    }
}