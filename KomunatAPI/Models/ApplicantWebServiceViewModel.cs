﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace KomunatAPI.Models
{
    public class ApplicantWebServiceGetViewModel
    {


        public string NUMRI_PERSONAL { get; set; }
        public int? WEB_REQUEST_ID { get; set; }
        public string EMRI { get; set; }
        public string MBIEMRI { get; set; }
        public string EMRI_MBIEMRI_BABA { get; set; }
        public string ADRESA_E_VENDBANIMIT { get; set; }
        public string VENDLINDJA { get; set; }
        public string DATLINDJA { get; set; }
        public string DATELINDJA_NENA { get; set; }
        public string GJINIA { get; set; }
        public string GJENDJA_MARTESORE { get; set; }
        public string SHTETESIA { get; set; }
        public string DATELINDJA_BABA { get; set; }
        public string VENDLINDJA_BABA { get; set; }
        public string EMRI_MBIEMRI_NENA { get; set; }
        public string VENDLINDJA_NENA { get; set; }
        public string VERIFIKUAR { get; set; }
        public string KALINDJE { get; set; }
        public string KAVDEKJE { get; set; }
        public string KAMARTESE { get; set; }
        public IList<CertificateItem> CertificateItems { get; set; }
        public string DELIVERED_ADDRESS { get; set; }
        public int MunicipalityWebId { get; set; }
        public string KOMUNA { get; set; }
        public string ADDRESSA { get; set; }
        public string TELFAX { get; set; }
        public string TELMOBILE { get; set; }
        public string EMAIL { get; set; }

        internal bool Validate()
        {
            if (String.IsNullOrEmpty(NUMRI_PERSONAL))
            {
                return false;
            }

            return true;
        }
    }

    public class ApplicantWebServicePostViewModel
    {
        public ApplicantWebServicePostViewModel()
        {

        }

        public string NUMRI_PERSONAL { get; set; }
        public int? WEB_REQUEST_ID { get; set; }
        public string EMRI { get; set; }
        public string MBIEMRI { get; set; }
        public string EMRI_MBIEMRI_BABA { get; set; }
        public string ADRESA_E_VENDBANIMIT { get; set; }
        public string VENDLINDJA { get; set; }
        public string DATLINDJA { get; set; }
        public string DATELINDJA_NENA { get; set; }
        public string GJINIA { get; set; }
        public string GJENDJA_MARTESORE { get; set; }
        public string SHTETESIA { get; set; }
        public string DATELINDJA_BABA { get; set; }
        public string VENDLINDJA_BABA { get; set; }
        public string EMRI_MBIEMRI_NENA { get; set; }
        public string VENDLINDJA_NENA { get; set; }
        public string VERIFIKUAR { get; set; }
        public string KALINDJE { get; set; }
        public string KAVDEKJE { get; set; }
        public string KAMARTESE { get; set; }
        public string DELIVERED_ADDRESS { get; set; }

        public int MunicipalityWebId { get; set; }

        public string KOMUNA { get; set; }
        public string ADDRESSA { get; set; }
        public string TELFAX { get; set; }
        public string TELMOBILE { get; set; }
        public string EMAIL { get; set; }

        public IList<CertificateItem> CertificateItems { get; set; }

        internal bool Validate()
        {
            if (String.IsNullOrEmpty(NUMRI_PERSONAL))
            {
                return false;
            }
            else if (WEB_REQUEST_ID == null ||
                     WEB_REQUEST_ID == 0)
            {
                return false;
            }
            foreach (var item in CertificateItems)
            {
                if (String.IsNullOrEmpty(item.Id.ToString()) ||
                    !decimal.TryParse(item.Id.ToString(), out decimal value)
                    )
                {
                    return false;
                }
            }

            return true;
        }
    }
}