﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KomunatAPI.Models
{
    public class CertificateWebJobViewModel
    {
         
        public int Id { get; set; }
        public int WEB_REQUEST_ID { get; set; }
        public int Certificate_id { get; set; }
        public string LastStatusChange { get; set; }
        public string protocolNr { get; set; }
        public int MunicipalityWebId { get; set; }
        public string NUMRI_PERSONAL { get; set; }
        public string HashPass { get; set; }
        public int Rdstatus { get; set; }
        public int Rstatus { get; set; }
    }
    public class CertificateWebJobViewModelList
    {
        public CertificateWebJobViewModelList()
        {
            CertificateWebJobViewModels = new List<CertificateWebJobViewModel>();
        }
        public List<CertificateWebJobViewModel> CertificateWebJobViewModels { get; set; }
    }
}