﻿using Microsoft.Web.Http.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Routing;

namespace KomunatAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.MapHttpAttributeRoutes();
            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
                            {
                                ["apiVersion"] = typeof( ApiVersionRouteConstraint )
                            }
            };
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning();

            // Web API configuration and services

            // Web API routes


            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes
                  .Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}
